#pragma config OSC = HS         // Oscillator Selection bits (XT oscillator)
#pragma config PWRT = ON        // Power-up Timer Enable bit (PWRT enabled)
#pragma config BOR = OFF        // Brown-out Reset Enable bit (Brown-out Reset disabled)
#pragma config WDT = OFF        // Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
#pragma config LVP = OFF        // Low Voltage ICSP Enable bit (Low Voltage ICSP disabled)

#pragma config CCP2MUX = OFF    // Clear for use RB3 as PWM output

/*---------------------------------------------------------*/
#include <xc.h>
#include <string.h>
#include <stdio.h>
#define _XTAL_FREQ 16000000
#include "lcd_i2c.h"

/* Constants */
// (PWM period / (Tcy * TMR2 prescale value)) - 1 = PR2
#define period_pr2(pwm_period, frec_osc, presc_tmr2) ((pwm_period / ((4 / frec_osc) * presc_tmr2))-1)
// PWM duty cycle / TOSC * (TMR2 prescale value) = (CCPR1L:CCP1CON<5:4>)
#define duty_ccp(pwm_period, pwm_duty, frec_osc, presc_tmr2) (((pwm_duty * pwm_period) / 100) / ((1 / frec_osc) * presc_tmr2))

#define dataTX TXREG

#define right_out3 LATAbits.LATA0
#define right_out4 LATAbits.LATA1

#define left_out1 LATAbits.LATA2
#define left_out2 LATAbits.LATA3


/* Global Vriables */
unsigned int cont_rc = 0, cont_ble = 0; // contador recibidor bits, flag data lista.
char rc_buff[100], rc_data[255], test_buff[30], cmd_buff[10];
unsigned char data = 0;

/* Variables PWM module*/
unsigned int mask = 0x03, periodo = 0, duty_temp = 0;
float pwm_p, frec_osc, tmr2_presc = 0;
unsigned char valor_prueba_debug, valor_prueba_debug2;

/* Prototype PWM */
void globalPWM(float period_pwm, float freq_osc, float presc); // config global module.
void change_duty(float pwm_duty, int module); // change duty cycle in CCP.
void setPWM(float period_pwm, float pwm_duty, float freq_osc, float presc, int module); // config and init PWM in CCP1

unsigned int isRight0 = 0, isRight100 = 0;

/* Prototype functions for UART comunication. */
void boot();
void init_uart(void);
void tx_stop(); // Disabled transmission for TX.
void tx_start();
void rx_stop(); // Disabled transmission for TX.
void rx_start();
void tx_write(unsigned char data); // Write data.
void tx_write_string(char *st);
void init_interrupts(void);

/* Motor Derecho functions */
void adelanteL(void);
void atrasL(void);
void stopL(void);

// Left motor.

void adelanteL(void) {
    left_out1 = 1;
    left_out2 = 0;
}

void atrasL(void) {
    left_out1 = 0;
    left_out2 = 1;
}

void stopL(void) {
    left_out1 = 1;
    left_out2 = 1;
}

// Right motor.

void adelanteR(void) {
    right_out3 = 0;
    right_out4 = 1;
}

void atrasR(void) {
    right_out3 = 1;
    right_out4 = 0;
}

void stopR(void) {
    right_out3 = 1;
    right_out4 = 1;
}


// PWM functions

void globalPWM(float period_pwm, float freq_osc, float presc) {
    // Periodo igual en los dos o eso dice el datasheet
    periodo = (unsigned int) period_pr2(period_pwm, freq_osc, presc);
    PR2 = periodo;
    //T2CON = 0x07;
}

void setPWM(float period_pwm, float pwm_duty, float freq_osc, float presc, int module) {
    /*
    CCPxCON -> registro de seleccion modo, ademas de selecconar el DUTY CYCLE del PWM, puede ser CCP1CON o CCP2CON
    PR2 -> registro donde se almacena nuestro periodo PWM para realizar la comparacion con el TMR2.

     * Tosc (periodo oscilacion) = 1/Fosc
     * Tcy  (Ciclo de maquina)   = 4/Fosc

     * PWM period = [(PR2) + 1] * 4 * TOSC * (TMR2 prescale value)
     PWM period = [(PR2) + 1] * Tcy * (TMR2 prescale value)
     PWM period / Tcy * (TMR2 prescale value) = (PR2 + 1)
     (PWM period / (Tcy * TMR2 prescale value)) - 1 = PR2

     PWM duty cycle = (CCPR1L:CCP1CON<5:4>) * TOSC * (TMR2 prescale value)
     PWM duty cycle / TOSC * (TMR2 prescale value) = (CCPR1L:CCP1CON<5:4>)

     */

    // save data global in the module PWM.




    // Seleccionando modulo para su configuracion.
    switch (module) {

        case 1:
            mask = 0x03;
            pwm_p = period_pwm;
            frec_osc = freq_osc;
            tmr2_presc = presc;


            //periodo = (unsigned int) period_pr2(period_pwm, freq_osc, presc);


            // Dutycycle.
            // para un ciclo del 50% (204.91us) el valor de (CCPR1L:CCP1CON<5:4>) es:
            // (CCPR1L:CCP1CON<5:4>) = 256.
            // mask is 0b0000 0011 << 4 --> 0b0011 0000
            duty_temp = (unsigned int) duty_ccp(period_pwm, pwm_duty, freq_osc, presc);
            mask &= duty_temp;
            mask = mask << 4;
            CCP1CON |= mask;

            CCPR1L = duty_temp >> 2;

            valor_prueba_debug = 0x00;
            valor_prueba_debug2 = 0x00;

            valor_prueba_debug |= CCP1CON;
            valor_prueba_debug2 |= CCPR1L;

            // CCP1 config how output
            TRISCbits.RC2 = 0;

            // Config TMR2 with T2CON
            // T2CONbits.TMR2ON(bit 2) is bit for set enable or reset disable the timer.
            // t2con 0b0000 0111 - presc 16 and not use postcaler.
            T2CON = 0x07;

            // CCP1 config PWM mode.
            CCP1CON |= 0x0F;
            break;
        case 2:
            mask = 0x03;
            //pwm_p = period_pwm;
            //frec_osc = freq_osc;
            //tmr2_presc = presc;


            //periodo = (unsigned int) period_pr2(period_pwm, freq_osc, presc);


            // Dutycycle.
            // para un ciclo del 50% (204.91us) el valor de (CCPR1L:CCP1CON<5:4>) es:
            // (CCPR1L:CCP1CON<5:4>) = 256.
            // mask is 0b0000 0011 << 4 --> 0b0011 0000
            duty_temp = (unsigned int) duty_ccp(period_pwm, pwm_duty, freq_osc, presc);
            mask &= duty_temp;
            mask = mask << 4;
            CCP2CON |= mask;

            CCPR2L = duty_temp >> 2;


            // ciento que es solo de prueba xd.
            valor_prueba_debug = 0x00;
            valor_prueba_debug2 = 0x00;

            valor_prueba_debug |= CCP2CON;
            valor_prueba_debug2 |= CCPR2L;

            // CCP2 config how output
            // CCP2MUX = OFF is clear for use RB3 how CCP2
            TRISBbits.RB3 = 0;

            // Config TMR2 with T2CON
            // T2CONbits.TMR2ON(bit 2) is bit for set enable or reset disable the timer.
            // t2con 0b0000 0111 - presc 16 and not use postcaler.
            T2CON = 0x07;

            // CCP1 config PWM mode.
            CCP2CON |= 0x0F;
            break;
    }
}

void change_duty(float pwm_duty, int module) {
    mask = 0x03;
    if (module == 1) {
        duty_temp = (unsigned int) duty_ccp(pwm_p, pwm_duty, frec_osc, tmr2_presc);
        mask &= duty_temp;
        mask = mask << 4;
        CCP1CON |= mask;

        CCPR1L = duty_temp >> 2;
    }
    if (module == 2) {
        duty_temp = (unsigned int) duty_ccp(pwm_p, pwm_duty, frec_osc, tmr2_presc);
        mask &= duty_temp;
        mask = mask << 4;
        CCP2CON |= mask;

        CCPR2L = duty_temp >> 2;
    }
}

// Config interrupts.

void init_interrupts(void) {
    RCONbits.IPEN = 0; // disabled interrupts with mask.
    INTCONbits.GIE = 1; // enabled unmasked interrupts.
    INTCONbits.PEIE = 1; // enabled pheriperal interrupts unmasked.

    // config interrupts for USART.
    PIE1bits.TXIE = 0; // disabled Trasmit interrupt.
    PIE1bits.RCIE = 1; // Enabled reception interrupt.

}

/* NOTAS
 * TXI is Trasmit interrupt, enable with TXIE

 * TXIF (PIR1<4>) set when TXREG is empty, TXREG is used to store the data in software, when TXREG is 
    full in one Tcy the TSR is charge with data in TXREG in this moment TXREG is empty and TXIF is set.

 * TMRT is a bit only read, set when data in TSR is empty, clear when data storege in register 
    no have interrupt logic.



 */

// UART functions

void rx_start(void) {
    RCSTAbits.CREN = 1;
}

void rx_stop(void) {
    RCSTAbits.CREN = 0;
}

void init_uart(void) {
    TXSTAbits.BRGH = 1; // 1 is high speed in asyncronus
    SPBRG = 103; // Select the baud rate with formula in datasheet.

    TXSTAbits.SYNC = 0; // USART MODE SELECTION BIT, 0 is asyncronus mode. 
    RCSTAbits.SPEN = 1; // SERIAL PORT ENABLED BIT, Serial port enabled (configures RC7/RX/DT and RC6/TX/CK pins as serial port pins)

    TRISC6 = 1; // As stated in the datasheet
    TRISC7 = 1; // As stated in the datasheet
    tx_start();
    rx_start();
}

void tx_start(void) {
    TXSTAbits.TXEN = 1; // Transmit Enable bit, 1 = Transmit enabled.
}

void tx_stop(void) {
    TXSTAbits.TXEN = 0; // Transmit Enable bit, 0 = Transmit disabled.
}

void tx_write(unsigned char data) {
    // TRMT bits is set when TSR is empty.

    while (!TXSTAbits.TRMT) {
        continue; // TXIF is flag to the bus TXREG, when TEXREG is empty TXIF is set.
    }

    dataTX = data; // USART TXREG register for data.
}

void tx_write_string(char *st) {
    // "1234\"
    /*
        Probar setear el flag para ver si inicia la interrupcion.
        y desde el ISR gestionar el envio de dato.

     * cuando se setea TXEN se habilita la trasmision, quiza cuando se habilite
        se empieze  aactivar la interrupcion.

     */
    for (int w = 0; st[w] != '\0'; w++) {
        tx_write(st[w]);
    }
}

/* Main Program! */

void boot() {
    init_interrupts();
    init_uart();
    I2C_Master_Init();
    globalPWM(1e0 / 2e3, 16e6, 16e0);


    ADCON1 = 0x06; // Registro ADCON1 para ADC, configuracion pines como I/O
    TRISA = 0x00; // TRISA como salidas

}

int main() {
    boot();
    LCD_Init(0x4E);
    LCD_Set_Cursor(1, 1);
    LCD_Write_String("Robot!");


    /*
     
     setPWM(period prefer in fraccion con notacion, duty en porcentaje entero, frecuencia osc notacion, prescaler prefer in notacion,
     module in int value (1 for CCP1, 2 for CCP2))
     
     */
    setPWM(1e0 / 2e3, 100, 16e6, 16e0, 1); // derecho
    setPWM(1e0 / 2e3, 100, 16e6, 16e0, 2); // izquierdo
    //adelanteL();
    //adelanteR();
    cont_rc = 0;

    // STOP MOTORS.
    stopL();
    stopR();

    int cont_pwm, cont_tr = 0;


    while (1) {
        
        //LCD_Set_Cursor(1, 1);
        //LCD_Write_String("ADELANTE!");

        cont_pwm = 0;
        cont_tr = 0;

        
        if(isRight0 == 1){
            //stopR();
            isRight0 = 0;
        }
         
        if(isRight100 == 1){
            //adelanteR();
            change_duty(100, 1);
            change_duty(100, 2);
            isRight100 = 0;
        }
        
        /*
        if (rc_buff == 'S'){
                    stopL();
                    stopR();
                    cont_ble = 0;
                    //LCD_Set_Cursor(1,1);
                    //LCD_Write_String(rc_buff);
                    continue;
                    //break;
                }
                else if(rc_buff == 'R'){
                    adelanteL();
                    adelanteR();
                    cont_ble = 1;
                    //LCD_Set_Cursor(1,1);
                    //LCD_Write_String(rc_buff);
                    continue;
                    //break;
                }
         */

        //LCD_Clear();

        /*
            if (cont_ble == 1){
                // PWM right.
                change_duty(atoi(rc_data), 1);
                        
                cont_ble = 2;
                //LCD_Set_Cursor(2,1);
                //LCD_Write_String(rc_data);
                continue;
                //break;
            }
            if(cont_ble == 2){
                change_duty(atoi(rc_data), 2);
                cont_ble = 0;
                //LCD_Set_Cursor(1,1);
                //LCD_Write_String(rc_data);
 
                continue;
                //break;
            }
         */


        //LCD_Set_Cursor(1,1);
        //LCD_Write_String(rc_data);
        /*
        adelanteL();
        adelanteR();
        __delay_ms(5000);
        atrasL();
        atrasR();
        __delay_ms(5000);
        stopL();
        stopR();
        __delay_ms(5000);
         */

    }

    return 0;
}

void __interrupt() adint(void) {

    if (PIR1bits.RCIF == 1) {
        // the RCREG is full. ready for read!
        PIR1bits.RCIF = 0;
        //LATDbits.LATD0 = 1;

        data = RCREG;
        //LCD_Set_Cursor(1,1);
        //sprintf(test_buff, "dato: %d", data);
        //LCD_Write_String(test_buff);




        //LCD_Set_Cursor(1,1);
        //LCD_Write_String(test_buff);



        switch (data) {
                // Predeterminadas.
            case 'R':

                adelanteL();
                adelanteR();
                LCD_Set_Cursor(1, 1);
                LCD_Write_String("ADELANTE_JOY!");
                data = 0;
                break;

            case 'B':
                atrasL();
                atrasR();
                data = 0;
                break;

            case 'S':

                stopL();
                stopR();
                data = 0;
                break;


                // PWM

                /*
                 * Derecha
                 30% -> C
                 40% -> I
                 50% -> D
                 60% -> A
                 70% -> L
                 80% -> Z
                 90% -> J
                 100% -> U 
             
                 */
            case 'C':
                //stopL();
                //stopR();
                change_duty(0, 1);
                change_duty(100, 2);
        
                
                //data = 0;
                break;

            case 'I':

                change_duty(40, 1);
                change_duty(100, 2);
                //stopL();
                //adelanteR();
                //data = 0;
                break;
                
            case 'D':
                change_duty(50, 1);
                change_duty(100, 2);
                break;
                
            case 'A':
                change_duty(60, 1);
                change_duty(100, 2);
                break;
            case 'L':
                change_duty(70, 1);
                change_duty(100, 2);
                break;

            case 'Z':
                change_duty(80, 1);
                change_duty(100, 2);
                break;

            case 'J':
                change_duty(90, 1);
                change_duty(100, 2);
                break;

            case 'U':
                change_duty(100, 1);
                change_duty(100, 2);
        
                break;

                // PWM

                /*
                 * Izquierda
                 30%  -> X
                 40%  -> K
                 50%  -> G
                 60%  -> V
                 70%  -> N
                 80%  -> M
                 90%  -> T
                 100% -> Y 
             
                 */
            case 'X':
                //stopL();
                //stopR();
                change_duty(100, 1); // derecho
                change_duty(0, 2); // izquierdo
                //data = 0;
                break;

            case 'K':
                //adelanteL();
                change_duty(100, 1);
                change_duty(40, 2);
                break;
            case 'G':
                change_duty(100, 1);
                change_duty(50, 2);
                break;

            case 'V':
                change_duty(100, 1);
                change_duty(60, 2);
                break;

            case 'N':
                change_duty(100, 1);
                change_duty(70, 2);
                break;

            case 'M':
                change_duty(100, 1);
                change_duty(80, 2);
                break;

            case 'T':
                change_duty(100, 1);
                change_duty(90, 2);
                break;

            case 'Y':
                change_duty(100, 1);
                change_duty(100, 2);
                break;

        }


    }

}