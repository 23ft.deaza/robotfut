import 'dart:ffi';
import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bluetooth_serial/flutter_bluetooth_serial.dart';
import './joypad.dart';

Future main() async {
  /*
  await SystemChrome.setPreferredOrientations([
    DeviceOrientation.landscapeLeft,
    DeviceOrientation.landscapeRight,
  ]);

  // disable all UI overlays (show fullscreen)
  await SystemChrome.setEnabledSystemUIOverlays([]);
  */
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.landscapeLeft])
      .then((_) {
    runApp(MaterialApp(home: RobotApp()));
  });
}

class RobotApp extends StatefulWidget {
  late BluetoothConnection connection;
  late FlutterBluetoothSerial bluetooth;
  List<BluetoothDevice> _devicesList = [];
  BluetoothDevice? _device;
  bool _connected = false;
  bool _pressed = false;

  var _pwmR = 0;
  var _pwmL = 0;
  var _pwmRD = 0;
  var _pwmLD = 0;
  var _preData = "";

  @override
  _RobotApp createState() => _RobotApp();
}

class _RobotApp extends State<RobotApp> {
  late TextEditingController _controller;

  /* Variables envio datos algoritmo */
  var data = "";
  var cont_send = 0;

  /* Flagas joy left*/
  bool _isRun = false;
  bool _isBack = false;
  bool _isStop = false;
  bool _noDirection = false;
  bool _modeLeft = false;
  bool _modeLeft1 = false;
  bool _modeRight = false;
  bool _modeRight1 = false;

  /* Flagas joy Right (direction joy)*/
  Map<int, bool> _IsSendR = {};
  Map<int, bool> _IsSendL = {};

  @override
  void initState() {
    super.initState();

    /* Fill map with flags in pwm rifht*/
    int _cont = 0;
    for (int x = 0; x < 8; x++) {
      _cont += 3;

      _IsSendR[_cont] = false;
      _IsSendL[_cont] = false;
    }

    widget.bluetooth = FlutterBluetoothSerial.instance;
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Future<void> bluetoothConnectionState(
      FlutterBluetoothSerial obj, List<BluetoothDevice> lista) async {
    try {
      lista = await obj.getBondedDevices();
      print("LISTA DEVICES: ${lista[0].address}");

      BluetoothConnection.toAddress(lista[0].address).then((conn) async {
        widget.connection = conn;
        widget.connection.output
            .add(Uint8List.fromList(utf8.encode("b" + "~")));

        widget.connection.input!.listen(_onDataRecibed);

        showDialog(
            context: context,
            builder: (context) {
              return AlertDialog(
                content: Text("Conectado!"),
              );
            });
      });
    } catch (err) {
      print("Error");
    }
  }

  void _sendJoyOff(Offset cordenadas) async {
    if (cordenadas.dy.round() < 0) {
      // Adelante

      if (_isRun) {
        _isRun = true;
        _isBack = false;
        _isStop = false;
      } else {
        _isRun = true;
        _isBack = false;
        _isStop = false;

        print("[Joy Send] data: R");
        _sendMessage("R");
      }
    } else if (cordenadas.dy.round() > 0) {
      // Atras

      if (_isBack) {
        _isRun = false;
        _isBack = true;
        _isStop = false;
      } else {
        _isRun = false;
        _isBack = true;
        _isStop = false;

        print("[Joy Send] data: B");
        _sendMessage("B");
      }
    } else if (cordenadas.dy.round() == 0) {
      // Stop
      if (_isStop) {
        _isRun = false;
        _isBack = false;
        _isStop = true;
      } else {
        _isRun = false;
        _isBack = false;
        _isStop = true;

        print("[Joy Send] data: S");
        _sendMessage("S");
      }
    }
  }

  // Enviar mensaje desde input.
  void _inputSend() async {
    print("Campo de texto: ${_controller.text.trim()}");
    _sendMessage(_controller.text.trim());
  }

  // CallBack data recibed.
  void _onDataRecibed(Uint8List data) async {
    //String _decode = String.fromCharCodes(data);
    print("Data recibida: ${data}");

    // [13, 10, 99, 111, 108, 111, 109, 98, 105, 97, 50, 48]
  }

  // Function to send sms to device connected.
  void _sendMessage(String sms) async {
    widget.connection.output.add(Uint8List.fromList(utf8.encode(sms)));
  }

  // Connect HC with funcion async bluetoothConnectionState()
  void _connectHC() async {
    bluetoothConnectionState(widget.bluetooth, widget._devicesList);
  }

  void _IsRight(Offset delta) {
    /*
                    * Derecha
                        24 -> C
                        21 -> I
                        18 -> D
                        15 -> A
                        12 -> L
                        9 -> Z
                        6 -> J
                        3 -> U

                        0 - 30 
             
                    */
    int value = delta.dx.round();

    if (value > 0) {
      _modeLeft = false;
      _modeLeft1 = false;
      _noDirection = false;
      if (value <= 1) {
        if (_modeRight) {
        } else {
          _modeRight = true;
          _modeRight1 = false;
          _sendMessage('U');

          print("[Joy Send R] data: U");
        }
      } else if (value >= 2) {
        if (_modeRight1) {
        } else {
          _modeRight1 = true;
          _modeRight = false;

          _sendMessage('C');
          //_nullMapSendR(6);
          print("[Joy Send R] data: C");
        }
      }
    }
  }

  void _IsLeft(Offset delta) async {
    int value = delta.dx.round();

    if (value < 0) {
      _modeRight = false;
      _modeRight1 = false;
      _noDirection = false;
      if (value >= -2) {
        if (_modeLeft) {
        } else {
          _modeLeft = true;
          _modeLeft1 = false;
          _sendMessage('Y');
          //_nullMapSendL(3);
          print("[Joy Send L] data: Y");
        }
      } else if (value <= -3) {
        if (_modeLeft1) {
        } else {
          _modeLeft = false;
          _modeLeft1 = true;

          _sendMessage('X');
          //_nullMapSendL(6);
          print("[Joy Send L] data: X");
        }
      }
    }
  }

/*
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(TextField(
          controller: _controller,

        child:           
          onSubmitted: (String value) async {
            await showDialog<void>(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Thanks!'),
                  content: Text(
                      'You typed "$value", which has length ${value.characters.length}.'),
                  actions: <Widget>[
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('OK'),
                    ),
                  ],
                );
              },
            );
          },
        ),
      ),
    );
  }
*/

  Widget build(BuildContext context) {
    // Scaffold es un layout para la mayoría de los Material Components.
    return Scaffold(
      backgroundColor: Colors.black,
      // el body es la mayor parte de la pantalla.
      body: Center(
        child: Container(
          color: Colors.grey,
          margin: const EdgeInsets.all(2.0),
          padding: const EdgeInsets.all(14.0),
          child: Column(children: [
            Spacer(),
            IconButton(
                color: Colors.black,
                iconSize: 50,
                onPressed: _connectHC,
                icon: const Icon(Icons.bluetooth_drive)),
            Spacer(),
            TextField(
              decoration: const InputDecoration(
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0)),
                  focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0)),
                  border: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey, width: 1.0))),
              controller: _controller,
              cursorColor: Colors.black,
            ),
            IconButton(
              icon: Icon(Icons.send_sharp),
              tooltip: 'Send message',
              onPressed: _inputSend,
            ),
            Spacer(),
            Row(
              children: [
                SizedBox(width: 48),
                Joypad(
                  /* Joy Left */
                  onChange: (Offset delta) => _sendJoyOff(delta),
                ),
                Spacer(),
                Joypad(
                  /* Joy Right */

                  onChange: (Offset delta) async {
                    // derecha.
                    _IsRight(delta);
                    // Izquierda
                    _IsLeft(delta);

                    // center
                    if (delta.dx.round() == 0) {
                      if (_noDirection) {
                      } else {
                        _sendMessage("U");

                        _modeLeft = false;
                        _modeLeft1 = false;
                        _modeRight = false;
                        _modeRight1 = false;

                        _noDirection = true;
                      }
                    }
                  },
                ),
                SizedBox(width: 48),
              ],
            ),
            SizedBox(height: 24),
          ]),
          /*
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center, // x
                crossAxisAlignment: CrossAxisAlignment.center, // y
                children: <Widget>[
                  TextField(
                    decoration: const InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.0)),
                        focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.0)),
                        border: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey, width: 1.0))),
                    controller: _controller,
                    cursorColor: Colors.black,
                  ),
                  IconButton(
                    icon: Icon(Icons.send_sharp),
                    tooltip: 'Send message',
                    onPressed: _inputSend,
                  )
                ])*/
        ),
      ),
      /*
      floatingActionButton: FloatingActionButton(
        backgroundColor: Colors.black,
        tooltip:
            'Connect', // utilizado por las tecnologías de accesibilidad para discapacitados
        child: const Icon(Icons.bluetooth_drive),
        onPressed: _connectHC,
      ),*/
    );
  }
}
